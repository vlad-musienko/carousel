var carousel1 = new Carousel({
    "carouselName": "carousel1",
    "imageWidth": 500,
    "imageHeight": 250,
    "images": [
        'images/1.jpg',
        'images/2.jpg',
        'images/3.jpg',
        'images/4.png',
        'images/5.jpg'
    ]
});

var carousel2 = new Carousel({
    "carouselName": "carousel2",
    "images": [
        'images/3.jpg',
        'images/4.png',
        'images/5.jpg'
    ],
    "imageWidth": 500,
    "imageHeight": 250
});

var carousel3 = new Carousel({
    "carouselName": "carousel3",
    "imageWidth": 500,
    "imageHeight": 250,
    "images": [
        'images/3.png',
        'images/1.jpg',
        'images/2.jpg',
        'images/4.png',
        'images/5.jpg'
    ]
});


var carousel4 = new Carousel({
    "carouselName": "carousel4",
    "imageWidth": 500,
    "imageHeight": 250,
    "images": [
        'images/4.png',
        'images/3.jpg',
        'images/5.jpg',
        'images/3.png'
    ]
});



carousel1.renderCarousel();
carousel2.renderCarousel();
carousel3.renderCarousel();
carousel4.renderCarousel();