//For working with bind in ie < 9
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function () {
            },
            fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis
                        ? this
                        : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

var Carousel = function (properties) {
    if($("#" + properties.carouselName).length != 0 || properties.carouselName == undefined){
        return alert("Неверное имя карусели");
    }
    this.carouselName = properties.carouselName;
    this.images = properties.images || [];
    this.imageWidth = properties.imageWidth || 500;
    this.imageHeight = properties.imageHeight || 250;
    this.currentImage = 0;
};

Carousel.prototype.nextImage = function () {
    if (this.currentImage < this.images.length - 1) {
        this.moveTo(++this.currentImage, true);
    }
    else {
        this.currentImage = 0;
        this.moveTo(this.currentImage, false);
    }
};

Carousel.prototype.prevImage = function () {
    if (this.currentImage > 0) {
        this.moveTo(--this.currentImage, true);
    }
    else {
        this.currentImage = this.images.length - 1;
        this.moveTo(this.currentImage, false);
    }
};

Carousel.prototype.insertPage = function (imageUrl, idx) {
    if (idx <= this.images.length) {
        this.images.splice(idx, 0, imageUrl);
        this.renderCarousel();
        this.moveTo(idx);
        this.currentImage = idx;
    }
    else {
        alert("Введенный индекс за пределами массива");
    }
};

Carousel.prototype.calcWidth = function () {
    this.containerWidth = this.images.length * this.imageWidth;
    $(this.carousel).find('.carousel-container').css("width", this.containerWidth);
};

Carousel.prototype.renderItem = function (item, id) {
    var img = $('<img class="carousel-image">');
    var navBtn = $('<span class="carousel-nav-btn"></span>');
    img.attr('src', item);
    img.attr('id', "image" + id);
    img.css({
        "width": this.imageWidth,
        "height": this.imageHeight
    });
    navBtn.attr('id', 'nav' + id);
    $(this.carousel).find('.carousel-container').append(img);
    $(this.carousel).find('.carousel-controls').append(navBtn);
};

Carousel.prototype.getPages = function () {
    return this.images;
};

Carousel.prototype.getPage = function (pageNum) {
    return this.images[pageNum] || "Нет такой страницы";
};

Carousel.prototype.addPage = function (page) {
    this.insertPage(page, this.images.length);
};

Carousel.prototype.removeItem = function (pageNum) {
    if (pageNum <= this.images.length) {
        this.images.splice(pageNum, 1);
        this.renderCarousel();
        return true;
    }
    else {
        return false;
    }
};


Carousel.prototype.moveTo = function (slideId, animate) {
    if (slideId <= this.images.length)
        var offset = slideId * this.imageWidth;
    this.currentImage = slideId;
    if (animate) {
        $(this.carousel).find(".carousel-container").animate({"right": offset}, 500);
    }
    else {
        $(this.carousel).find(".carousel-container").css("right", offset);
    }
    $(this.carousel).find('.active').removeClass('active');
    $(this.carousel).find('#nav' + slideId).addClass('active');
};

Carousel.prototype.controlsHandler = function (event) {
    var target = $(event.target);
    if (target.prop("tagName") == "SPAN") {
        var id = target.attr('id').substr(3);
        this.moveTo(id, true);
    }
};

Carousel.prototype.renderCarousel = function () {
    //search container
    var carousel = $('.carousel#' + this.carouselName);

    if (carousel.length == 0) {
        carousel = $('<div class="carousel"></div>"').attr("id", this.carouselName);
        $('.wrapper').append(carousel);
    } else {
        carousel.html("");
    }
    this.carousel = carousel;
    var wrapper = $('<div class="carousel-wrapper"></div>"');
    var container = $('<div class="carousel-container"></div>"');
    var controls = $('<div class="carousel-controls"></div>"');
    var pervBtn = $('<span></span>').attr('id', 'carousel-prev');
    var nextBtn = $('<span></span>').attr('id', 'carousel-next');
    var clear = $('<div style="clear:both"></div>');
    carousel.css({
        "width": this.imageWidth,
        "height": this.imageHeight + 32
    });
    wrapper.append(container);
    wrapper.append(pervBtn);
    wrapper.append(nextBtn);
    carousel.append(wrapper);
    carousel.append(controls);
    this.calcWidth();
    $(carousel).find("#carousel-next").on('click', this.nextImage.bind(this));
    $(carousel).find("#carousel-prev").on('click', this.prevImage.bind(this));
    $(carousel).find('.carousel-controls').on('click', this.controlsHandler.bind(this));
    _.each(this.images, this.renderItem.bind(this));
    this.moveTo(0, false);
    this.calcWidth();
    carousel.append(clear);
};
